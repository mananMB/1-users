const users = {
  John: {
    age: 24,
    desgination: "Senior Golang Developer",
    interests: ["Chess, Reading Comics, Playing Video Games"],
    qualification: "Masters",
    nationality: "Greenland",
  },
  Ron: {
    age: 19,
    desgination: "Intern - Golang",
    interests: ["Video Games"],
    qualification: "Bachelor",
    nationality: "UK",
  },
  Wanda: {
    age: 24,
    desgination: "Intern - Javascript",
    interests: ["Piano"],
    qualification: "Bachaelor",
    nationality: "Germany",
  },
  Rob: {
    age: 34,
    desgination: "Senior Javascript Developer",
    interest: ["Walking his dog, Cooking"],
    qualification: "Masters",
    nationality: "USA",
  },
  Pike: {
    age: 23,
    desgination: "Python Developer",
    interests: ["Listing Songs, Watching Movies"],
    qualification: "Bachaelor's Degree",
    nationality: "Germany",
  },
};

//Fix Key
const fixedData = () => {
  return Object.entries(users).map((user) => {
    let fixedKeys = Object.entries(user[1]).map((userData) => {
      if (userData[0] === "interest") {
        return ["interests", userData[1]];
      } else {
        return userData;
      }
    });
    let newObject = Object.fromEntries(fixedKeys);
    return Object.fromEntries([[user[0], newObject]]);
  });
};

// Q1 Find all users who are interested in playing video games.
const filterInterests = (interest) => {
  return fixedData().filter((user) => {
    let interests = Object.entries(user)[0][1].interests[0].toLowerCase();
    return interests.includes(interest.toLowerCase());
  });
};

// Q2 Find all users staying in Germany.
const filterNationality = (nationalityKey) => {
  return fixedData().filter((user) => {
    let nationality = Object.entries(user)[0][1].nationality.toLowerCase();
    return nationality === nationalityKey.toLowerCase();
  });
};

// Q3 Sort users based on their seniority level
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10
const sort = (designationSortOrder) => {
  let sortedUser = fixedData().sort((userA, userB) => {
    return (
      designationSortOrder.indexOf(userA.desgination) -
      designationSortOrder.indexOf(userB.desgination)
    );
  });

  console.log(sortedUser);
};

sort(["Senior Developer", "Developer", "Intern"]);

// Q4 Find all users with masters Degree.
const filterDegree = (degreeKey) => {
  return fixedData().filter((user) => {
    let degree = Object.entries(user)[0][1].qualification.toLowerCase();
    return degree === degreeKey.toString();
  });
};

// Q5 Group users based on their Programming language mentioned in their designation.
